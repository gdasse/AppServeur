<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajouter document</title>
</head>
<body>
	<form method="post" action="ajouterDocument">
		<input type="text" name="Auteur" placeholder="Auteur"><br>
		<input type="text" name="Titre" placeholder="Titre"><br>
		<select name="Type">
			<option value="1">CD</option>
			<option value="2">DVD</option>
			<option value="3">Livre</option>
		</select>
		<button type="submit">Enregistrer le document</button>
	</form>
</body>
</html>
/**
 * Gauthier DASSÉ
 */
window.addEventListener("load", function(){
	document.getElementById("bouton__emprunter").addEventListener("click", function(){
		window.location = "http://localhost:8080/ProjetAppServ/emprunter"
	})
	
	document.getElementById("bouton__retourner").addEventListener("click", function(){
		window.location = "http://localhost:8080/ProjetAppServ/retour"
	})
	
		document.getElementById("bouton__reserver").addEventListener("click", function(){
		window.location = "http://localhost:8080/ProjetAppServ/reserver"
	})
	
	document.getElementById("bouton__deconnexion").addEventListener("click", function(){
		window.location = "http://localhost:8080/ProjetAppServ/deconnexion"
	})
	
	var ajouter = document.getElementById("bouton__ajouter")
	if(ajouter != null){
		ajouter.addEventListener("click", function(){
			window.location = "http://localhost:8080/ProjetAppServ/ajouter"
		})
	}
})
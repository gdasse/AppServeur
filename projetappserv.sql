-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 22 mars 2020 à 20:04
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetappserv`
--

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

DROP TABLE IF EXISTS `document`;
CREATE TABLE IF NOT EXISTS `document` (
  `idDoc` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(100) COLLATE utf8_bin NOT NULL,
  `Auteur` varchar(100) COLLATE utf8_bin NOT NULL,
  `Type` int(11) NOT NULL,
  PRIMARY KEY (`idDoc`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `document`
--

INSERT INTO `document` (`idDoc`, `Titre`, `Auteur`, `Type`) VALUES
(1, 'En attendant GODOT', 'Samuel Beckett', 3),
(2, 'A la recherche du temps perdu', 'Marcel Proust', 3),
(3, 'Personal Jesus', 'Depeche Mode', 1),
(4, 'Origin of Illness', 'Kill the Young', 1),
(5, 'L\'art de l\'insulte', 'Arthur Schopenhaeur', 3),
(6, 'Test', 'Test', 1);

-- --------------------------------------------------------

--
-- Structure de la table `emprunter`
--

DROP TABLE IF EXISTS `emprunter`;
CREATE TABLE IF NOT EXISTS `emprunter` (
  `idDoc` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`idDoc`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `reserver`
--

DROP TABLE IF EXISTS `reserver`;
CREATE TABLE IF NOT EXISTS `reserver` (
  `idDoc` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`idDoc`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `reserver`
--

INSERT INTO `reserver` (`idDoc`, `idUser`, `Date`) VALUES
(1, 1, '2020-03-22'),
(2, 1, '2020-03-22');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(20) COLLATE utf8_bin NOT NULL,
  `Login` varchar(20) COLLATE utf8_bin NOT NULL,
  `MDP` varchar(20) COLLATE utf8_bin NOT NULL,
  `Bibliothecaire` int(11) NOT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUser`, `Nom`, `Login`, `MDP`, `Bibliothecaire`) VALUES
(1, 'Ibn', 'a', 'a', 0),
(2, 'Aimen', 'b', 'b', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `emprunter`
--
ALTER TABLE `emprunter`
  ADD CONSTRAINT `emprunter_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `utilisateur` (`idUser`),
  ADD CONSTRAINT `emprunter_ibfk_2` FOREIGN KEY (`idDoc`) REFERENCES `document` (`idDoc`);

--
-- Contraintes pour la table `reserver`
--
ALTER TABLE `reserver`
  ADD CONSTRAINT `reserver_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `utilisateur` (`idUser`),
  ADD CONSTRAINT `reserver_ibfk_2` FOREIGN KEY (`idDoc`) REFERENCES `document` (`idDoc`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

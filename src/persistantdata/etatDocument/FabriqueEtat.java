package persistantdata.etatDocument;

import mediatek2020.items.Utilisateur;

public class FabriqueEtat {

	public static EtatDocument getEtatDocument(int idEtat, Utilisateur u, int idDoc) {
		switch (idEtat) {
		case 1:
			return new EtatEmprunter(u, idDoc);
		case 2:
			return new EtatReserver(u, idDoc);
		default:
			return new EtatLibre(idDoc);
		}
	}

}

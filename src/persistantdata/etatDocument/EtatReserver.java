package persistantdata.etatDocument;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import mediatek2020.items.EmpruntException;
import mediatek2020.items.ReservationException;
import mediatek2020.items.RetourException;
import mediatek2020.items.Utilisateur;
import persistantdata.ConnexionBDD;

public class EtatReserver extends EtatDocument {
	private Utilisateur u;
	private int idDoc;
	
	public EtatReserver(Utilisateur u, int idDoc) {
		this.u = u;
		this.idDoc = idDoc;
	}

	@Override
	public EtatDocument emprunter(Utilisateur u) throws EmpruntException {
		if(this.u.equals(u)) {
			try {
				removeDocumentReserver();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return new EtatEmprunter(u, idDoc);
		}
		throw new EmpruntException();
	}

	@Override
	public EtatDocument reserver(Utilisateur u) throws ReservationException {
		throw new ReservationException();
	}

	@Override
	public EtatDocument retour(Utilisateur u) throws RetourException {
		if(this.u.data()[0] == u.data()[0]) {
			try {
				removeDocumentReserver();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return new EtatLibre(idDoc);
		} else {
			throw new RetourException();
		}
	}
	
	private void removeDocumentReserver() throws SQLException {
		Connection con = ConnexionBDD.getConnection();
		PreparedStatement psmt = con.prepareStatement("Delete from RESERVER where idDoc = ?");
		psmt.setInt(1, idDoc);
		psmt.executeUpdate();
	}
	
	@Override
	public String getEtat() {
		return "Reserve";
	}
	
	@Override
	public Utilisateur getProprio() {
		return this.u;
	}
}

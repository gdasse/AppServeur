package persistantdata.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

import mediatek2020.items.Document;
import mediatek2020.items.EmpruntException;
import mediatek2020.items.ReservationException;
import mediatek2020.items.RetourException;
import mediatek2020.items.Utilisateur;
import persistantdata.ConnexionBDD;
import persistantdata.etatDocument.EtatDocument;

public abstract class ADocument implements Document {
	protected int idDoc;
	protected String nomAuteur;
	protected String titre;
	protected EtatDocument etatDocument;
	private Timer t;
	private final static int tempsReservation = 7200000;

	public ADocument(int idDoc, String nomAuteur, String titre, EtatDocument etatDocument) {
		this.idDoc = idDoc;
		this.nomAuteur = nomAuteur;
		this.etatDocument = etatDocument;
		this.titre = titre;
	}
	
	@Override
	public Object[] data() {
		Object[] user = new Object[5];
			user[0] = idDoc;
			user[1] = nomAuteur;
			user[2] = titre;
			user[3] = etatDocument.getEtat();
			user[4] = etatDocument.getProprio();
			return user;
	}

	@Override
	public void emprunter(Utilisateur u) throws EmpruntException {
			etatDocument.emprunter(u);
			Connection con = ConnexionBDD.getConnection();
			try {
				PreparedStatement pstmt = con.prepareStatement("INSERT INTO EMPRUNTER values (?,?,SYSDATE())");
				pstmt.setInt(1, idDoc);
				pstmt.setInt(2, (int) u.data()[0]);
				pstmt.executeUpdate();
			} catch (SQLException e) {
				//si on est ici, c'est qu'un utilisateur a emprunt� le document juste avant
				 throw new EmpruntException();
			}
	}

	@Override
	public void rendre(Utilisateur u) throws RetourException {
			etatDocument.retour(u);
	}

	@Override
	public void reserver(Utilisateur u) throws ReservationException {
			this.t = new Timer();
			t.schedule(new AnnulationReservation(u), tempsReservation);
			etatDocument.reserver(u);

			Connection con = ConnexionBDD.getConnection();
			try {
				PreparedStatement pstmt = con.prepareStatement("INSERT INTO RESERVER values (?,?,SYSDATE())");
				pstmt.setInt(1, idDoc);
				pstmt.setInt(2, (int) u.data()[0]);
				pstmt.executeUpdate();
			} catch (SQLException e) {
				throw new ReservationException();
			}
	}

	private class AnnulationReservation extends TimerTask {
		private Utilisateur u;

		private AnnulationReservation(Utilisateur u) {
			this.u = u;
		}

		@Override
		public void run() {
			try {
				Connection con = ConnexionBDD.getConnection();
				try {
					PreparedStatement pstm = con.prepareStatement("DELETE FROM RESERVER where idDoc = ?;");
					pstm.setInt(1, idDoc);
					pstm.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				etatDocument.retour(u);
			} catch (RetourException e) {
			}
		}
	}
}

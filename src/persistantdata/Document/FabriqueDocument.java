package persistantdata.Document;

import mediatek2020.items.Document;
import persistantdata.etatDocument.EtatDocument;

public class FabriqueDocument {
	
	public static Document createDocument(int idDoc, String nomAuteur, String titre,int type, EtatDocument etatDocument) {
		switch(type) {
		case 1:
			return new CD(idDoc, nomAuteur, titre, etatDocument);
		case 2:
			return new DVD(idDoc, nomAuteur, titre, etatDocument);
		case 3 :
			return new Livre(idDoc, nomAuteur, titre, etatDocument);
		default:
			return null;
		}
	}

}

package persistantdata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import mediatek2020.*;
import mediatek2020.items.Document;
import mediatek2020.items.Utilisateur;
import persistantdata.Document.FabriqueDocument;
import persistantdata.Utilisateur.User;
import persistantdata.etatDocument.*;

// classe mono-instance  dont l'unique instance est inject�e dans Mediatheque
// via une auto-d�claration dans son bloc static

public class MediathequeData implements PersistentMediatheque {
	// Jean-Fran�ois Brette 01/01/2018
	static {
		Mediatheque.getInstance().setData(new MediathequeData());
	}

	private MediathequeData() {
	}

	// renvoie la liste de tous les documents de la biblioth�que
	@Override
	public List<Document> tousLesDocuments() {
		Connection con = ConnexionBDD.getConnection();
		List<Document> listeDocuments = new LinkedList<Document>();
		try {
			PreparedStatement pstmt = con.prepareStatement(
					// r�cup�re tous les documents et cr�er un nombre EtatDoc en fonction de l'�tat
					// du document
					"SELECT * FROM document as d LEFT JOIN "
							+ "(Select r.idDoc, r.idUser, u.Nom, u.Login, u.Bibliothecaire, EtatDoc " + "from ("
							+ "SELECT idDoc, idUser, 1 as EtatDoc FROM emprunter " + "UNION "
							+ "SELECT idDoc, idUser, 2 as EtatDoc FROM reserver"
							+ ") as r INNER JOIN utilisateur as u ON r.idUser = u.idUser )"
							+ " as e on d.idDoc = e.idDoc");
			ResultSet res = pstmt.executeQuery();
			while (res.next()) {
				User u = null;
				Integer idUser = res.getInt("idUser");
				if (idUser != null) {
					u = new User(res.getInt("idUser"), res.getString("Nom"), res.getString("Login"),
							res.getBoolean("Bibliothecaire"));
				}
				EtatDocument etat = FabriqueEtat.getEtatDocument(res.getInt("EtatDoc"), u, res.getInt("idDoc"));
				listeDocuments.add(FabriqueDocument.createDocument(res.getInt("idDoc"), res.getString("Auteur"),
						res.getString("Titre"), res.getInt("Type"), etat));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeDocuments;
	}

	// va r�cup�rer le User dans la BD et le renvoie
	// si pas trouv�, renvoie null
	@Override
	public Utilisateur getUser(String login, String password) {
		try {
			Connection con = ConnexionBDD.getConnection();
			PreparedStatement pstmt = con.prepareStatement("Select * from Utilisateur where Login = ? AND MDP = ?");
			pstmt.setString(1, login);
			pstmt.setString(2, password);
			ResultSet res = pstmt.executeQuery();
			if (res.next()) {
				return new User(res.getInt("idUser"), res.getString("Nom"), res.getString("login"),
						res.getBoolean("bibliothecaire"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// va r�cup�rer le document de num�ro numDocument dans la BD
	// et le renvoie
	// si pas trouv�, renvoie null
	@Override
	public Document getDocument(int numDocument) {
		try {
			Connection con = ConnexionBDD.getConnection();
			PreparedStatement pstmt = con.prepareStatement(
					"SELECT * FROM document as d LEFT JOIN (Select r.idDoc, r.idUser, u.Nom, u.Login, u.Bibliothecaire, EtatDoc from (SELECT idDoc, idUser, 1 as EtatDoc FROM emprunter UNION SELECT idDoc, idUser, 2 as EtatDoc FROM reserver ) as r INNER JOIN utilisateur as u ON r.idUser = u.idUser ) as e on d.idDoc = e.idDoc where d.idDoc = ?");
			pstmt.setInt(1, numDocument);
			ResultSet res = pstmt.executeQuery();
			if (res.next()) {
				User u = new User(res.getInt("idUser"), res.getString("Nom"), res.getString("Login"),
						res.getBoolean("Bibliothecaire"));
				EtatDocument etat = FabriqueEtat.getEtatDocument(res.getInt("EtatDoc"), u, res.getInt("idDoc"));
				return FabriqueDocument.createDocument(res.getInt("idDoc"), res.getString("Auteur"),
						res.getString("Titre"), res.getInt("Type"), etat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void nouveauDocument(int type, Object... args) {
		try {
			Connection con = ConnexionBDD.getConnection();
			PreparedStatement pstmt = con.prepareStatement("Insert into Document(Titre,Auteur,Type) values(?,?,?)");
			pstmt.setString(1, (String) args[0]);
			pstmt.setString(2, (String) args[1]);
			pstmt.setInt(3, type);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

package services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Index extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session	= request.getSession(true);
		session.setAttribute("utilisateur",null);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Index.jsp").forward(request, response);
	}
}

package services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mediatek2020.Mediatheque;
import mediatek2020.items.Utilisateur;

public class Authentification extends HttpServlet {
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Class.forName("persistantdata.MediathequeData");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		String login = request.getParameter("login");
		String mdp = request.getParameter("MDP");
		Utilisateur u = Mediatheque.getInstance().getUser(login, mdp);
		if(u!=null) {
			HttpSession session	= request.getSession(true);
			session.setAttribute("utilisateur",u);
			response.sendRedirect("accueil");
		} else {
			response.sendRedirect("index");
		}
	}	
}

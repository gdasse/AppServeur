package services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mediatek2020.Mediatheque;
import mediatek2020.items.RetourException;
import mediatek2020.items.Utilisateur;

public class RetournerDocument extends HttpServlet {
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Accueil.estConnecte(request, response);
		Integer idDoc = Integer.parseInt(request.getParameter("idDoc"));
		HttpSession session	= request.getSession(true);
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		try {
			Mediatheque.getInstance().getDocument(idDoc).rendre(utilisateur);
		} catch (RetourException e) {
			System.out.println("Erreur retour");
		}
		response.sendRedirect("accueil");
	}

}

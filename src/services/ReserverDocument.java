package services;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mediatek2020.Mediatheque;
import mediatek2020.items.ReservationException;
import mediatek2020.items.Utilisateur;

public class ReserverDocument extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Accueil.estConnecte(request, response);
		Integer idDoc = Integer.parseInt(request.getParameter("idDoc"));
		HttpSession session	= request.getSession(true);
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		try {
			Mediatheque.getInstance().getDocument(idDoc).reserver(utilisateur);
		} catch (ReservationException e) {
			request.setAttribute("error", "Le document n�" + idDoc + " vient d'�tre pris par quelqu'un d'autre, la r�servation est donc annul�");
		}
		ServletContext context= getServletContext();
		context.getRequestDispatcher("/accueil").forward(request, response);
	}

}

package services;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mediatek2020.Mediatheque;
import mediatek2020.items.Document;
import mediatek2020.items.Utilisateur;

public class Reserver extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Accueil.estConnecte(request, response);
		HttpSession session	= request.getSession(true);
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		List<Document> listDocuments = Mediatheque.getInstance().tousLesDocuments();
		List<Document> documentDisponible = new LinkedList<Document>();
		for(Document d : listDocuments) {
			Object[] data = d.data();
			if("Libre".equals((String) data[3])) {
				documentDisponible.add(d);
			}
		}
		request.setAttribute("listeDocument",documentDisponible );
		this.getServletContext().getRequestDispatcher("/WEB-INF/Reserver.jsp").forward(request, response);
	}
}
